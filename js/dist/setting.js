"use strict";

// Back to top button - Set display on scroll amount
var scrollWindows = 200;
$(window).on('scroll', function () {
  if ($(this).scrollTop() > scrollWindows) {
    $('#back-to-top').addClass('display');
  } else {
    $('#back-to-top').removeClass('display');
  }
}); // Everything with class '.h-line' will be rendered in same height

jQuery(document).ready(function ($) {
  $('html').imagesLoaded(function () {
    $('.h-line').matchHeight();
  });
}); // Smooth scroll - Add class = "smooth-scroll" to <a>

jQuery(function ($) {
  $('a.smooth-scroll[href^="#"]').click(function (e) {
    e.preventDefault();
    var headerHeight = 0;
    var win = $(window).width();

    if (win < 980) {
      headerHeight = 60;
    }

    var speed = 400;
    var href = jQuery(this).attr('href');
    var target = jQuery(href == '#' || href == '' ? 'html' : href);
    var position = target.offset().top - headerHeight;
    $('body,html').animate({
      scrollTop: position
    }, speed, 'swing');
    return false;
  });
});
//# sourceMappingURL=setting.js.map
