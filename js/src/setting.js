// Back to top button - Set display on scroll amount
const scrollWindows = 200
$(window).on('scroll', function() {
  if ($(this).scrollTop() > scrollWindows) {
    $('#back-to-top').addClass('display')
  }
  else {
    $('#back-to-top').removeClass('display')
  }
})

// Everything with class '.h-line' will be rendered in same height
jQuery(document).ready(function($) {
  $('html').imagesLoaded(function() {
    $('.h-line').matchHeight()
  })
})

// Smooth scroll - Add class = "smooth-scroll" to <a>
jQuery(function($) {
  $('a.smooth-scroll[href^="#"]').click(function(e) {
    e.preventDefault()
    let headerHeight = 0
    const win = $(window).width()
    if (win < 980) {
      headerHeight = 60
    }
    const speed = 400
    const href = jQuery(this).attr('href')
    const target = jQuery(href == '#' || href == ''
                          ? 'html'
                          : href)
    const position = target.offset().top - headerHeight
    $('body,html').animate({ scrollTop: position }, speed, 'swing')
    return false
  })
})
