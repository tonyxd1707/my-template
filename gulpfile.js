const gulp = require('gulp'),
  plumber = require('gulp-plumber'),
  rename = require('gulp-rename'),
  ejs = require('gulp-ejs'),
  sass = require('gulp-sass'),
  sourcemaps = require('gulp-sourcemaps'),
  minifyCss = require('gulp-minify-css'),
  autoprefixer = require('gulp-autoprefixer'),
  babel = require('gulp-babel'),
  // If you want to concat all js file, enable gulp-concat
  // concat = require('gulp-concat'),
  fs = require('fs')

gulp.task('ejs', function() {
  const tmp_file = './ejs/template.ejs',
    json_file = './ejs/data/pages.json',
    json = JSON.parse(fs.readFileSync(json_file)),
    page_data = json.pages
  for (let i = 0; i < page_data.length; i++) {
    let id = page_data[i].id,
      parentId1 = page_data[i].parentId1,
      parentId2 = page_data[i].parentId2,
      parentId3 = page_data[i].parentId3,
      parentId4 = page_data[i].parentId4,
      depth = page_data[i].depth,
      RELATIVE_PATH = ''
    if (depth === 0) {
      RELATIVE_PATH = './'
    }
    else if (depth === 1) {
      RELATIVE_PATH = '../'
    }
    else if (depth === 2) {
      RELATIVE_PATH = '../../'
    }
    else if (depth === 3) {
      RELATIVE_PATH = '../../../'
    }
    else if (depth === 4) {
      RELATIVE_PATH = '../../../../'
    }
    if (parentId4 !== '') {
      parentId1 = parentId1 + '/' + parentId2 + '/' + parentId3 + '/' + parentId4
    }
    else if (parentId3 !== '') {
      parentId1 = parentId1 + '/' + parentId2 + '/' + parentId3
    }
    else if (parentId2 !== '') {
      parentId1 = parentId1 + '/' + parentId2
    }
    gulp.src(tmp_file)
        .pipe(plumber())
        .pipe(ejs({
          pageData: page_data[i],
          RELATIVE_PATH: RELATIVE_PATH
        }))
        .pipe(rename(id + '.html'))
        .pipe(gulp.dest('./' + parentId1))
  }
})

gulp.task('sass', () => {
  gulp.src('scss/**/*scss')
      .pipe(plumber())
      .pipe(sourcemaps.init())
      .pipe(sass({
        outputStyle: 'expanded'
      }))
      .pipe(autoprefixer({
        browsers: [
          'last 3 versions',
          'ie >= 10'
        ]
      }))
      .pipe(minifyCss({ advanced: false }))
      .pipe(sourcemaps.write('./maps'))
      .pipe(gulp.dest('./css'))
})

gulp.task('js', () =>
  gulp.src('js/src/**/*.js')
      .pipe(sourcemaps.init())
      .pipe(babel({
        presets: ['@babel/preset-env']
      }))
      // If you want to concat all js file, uncomment this line below
      // .pipe(concat('app.js'))
      .pipe(sourcemaps.write('.'))
      .pipe(gulp.dest('js/dist'))
)

gulp.task('watch', () => {
  gulp.watch('ejs/**/*.ejs', ['ejs'])
  gulp.watch('scss/*.scss', ['sass'])
  gulp.watch('js/src/**/*.js', ['js'])
})


